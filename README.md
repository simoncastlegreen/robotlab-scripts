# RobotLab-scripts

Scripts for kicking off initial config prior to Ansible deployment


*WORKSTATION*

To run, on a fresh install:  

wget https://tinyurl.com/rl-initws


wget https://gitlab.com/simoncastlegreen/robotlab-scripts/-/raw/main/init_ws.sh
chmod +x init_ws.sh
./init_ws.sh



*TURTLEBOT*

To run, on a fresh install:

wget https://tinyurl.com/rl-inittb


wget https://gitlab.com/simoncastlegreen/robotlab-scripts/-/raw/main/init_tb.sh
chmod +x init_tb.sh
./init_tb.sh