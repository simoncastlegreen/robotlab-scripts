#!/bin/bash

sudo apt update
sudo apt install openssh-server wget vim python -y
mkdir ~/.ssh/
echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMEIdx93/EsRHR1r6gMEVo2P0mMSpgCP8lf0+atwSgOP simon.castle-green@nottingham.ac.uk' >> ~/.ssh/authorized_keys

ip addr |grep inet

rm -f ./rl-initws
