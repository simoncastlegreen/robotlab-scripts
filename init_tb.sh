#!/bin/bash

mkdir ~/.ssh/
echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMEIdx93/EsRHR1r6gMEVo2P0mMSpgCP8lf0+atwSgOP simon.castle-green@nottingham.ac.uk' >> ~/.ssh/authorized_keys

rm -f ./rl-initws
wget https://www.edimax.com/edimax/mw/cufiles/files/download/Driver_Utility/EW-7811Un_V2/EW-7811Un_V2_Linux_Driver_1.0.1.3.zip
unzip EW-7811Un_V2_Linux_Driver_1.0.1.3.zip
cd EW-7811Un_V2_Linux_Driver_1.0.1.3
export ARCH=arm64
make
sudo make install
